package authtestsuite;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.net.URL;
import java.net.MalformedURLException;


/**
 * Created by kirif on 01.09.2017.
 */
public class BaseTestCase {

    public WebDriver driver;

    @BeforeMethod
    void init () throws MalformedURLException{
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
    }

    @AfterMethod
    void clear(){
        driver.quit();
    }

}
