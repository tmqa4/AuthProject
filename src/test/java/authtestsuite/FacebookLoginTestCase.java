package authtestsuite;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pageobjects.AuthPage;
import pageobjects.CabinetPage;
import pageobjects.IndexPage;
import pageobjects.popups.FacebookPopup;
import utility.services.WaiterService;
import utility.services.WebElementService;

/**
 * Created by kirif on 01.09.2017.
 */
public class FacebookLoginTestCase extends BaseTestCase {

    String url = "https://templatemonster.com";
    String email = "kirillk-prod51@templatemonster.me";
    String pass = "opa2008";

    @Test
    void test_001() {
        WaiterService service = new WaiterService(driver);
        WebElementService service2 = new WebElementService(driver);
        service2.goTo(url);

        IndexPage index = PageFactory.initElements(driver, IndexPage.class);
        index.closeBanner();
        index.clickOnSignInBtn();

        service2.switchToLastWindow();
        service.waitURL("account");

        AuthPage auth = PageFactory.initElements(driver, AuthPage.class);
        auth.clickOnFacebookBtn();
        service2.switchToLastWindow();
        service.waitURL("facebook");

        FacebookPopup facebook = PageFactory.initElements(driver, FacebookPopup.class);
        facebook.loginFacebook(email, pass);
        service2.switchToLastWindow();
        service.waitURL("products");

        CabinetPage cabinet = PageFactory.initElements(driver, CabinetPage.class);
        cabinet.isAccountMenuVisible();
    }
}
