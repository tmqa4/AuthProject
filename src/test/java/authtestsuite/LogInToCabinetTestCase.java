package authtestsuite;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.*;
import pageobjects.AuthPage;
import pageobjects.CabinetPage;
import pageobjects.IndexPage;
import utility.services.WaiterService;
import utility.services.WebElementService;


/**
 * Created by kirif on 27.08.2017.
 */
public class LogInToCabinetTestCase extends BaseTestCase {

    String url = "https://templatemonster.com";
    String email = "kirillk-prod51@templatemonster.me";
    String pass = "opa2008";

    @Test
    void test_002() {
        WebElementService service = new WebElementService(driver);
        WaiterService service2 = new WaiterService(driver);
        service.goTo(url);

        IndexPage index = PageFactory.initElements(driver, IndexPage.class);
        index.closeBanner();
        index.clickOnSignInBtn();
        service.switchToLastWindow();
        service2.waitURL("account");

        AuthPage account = PageFactory.initElements(driver, AuthPage.class);
        account.login(email, pass);
        service.switchToLastWindow();
        service2.waitURL("products");

        CabinetPage cabinet = PageFactory.initElements(driver, CabinetPage.class);
        cabinet.isAccountMenuVisible();
    }
}
