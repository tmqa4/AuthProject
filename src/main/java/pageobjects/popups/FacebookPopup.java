package pageobjects.popups;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utility.services.WaiterService;

/**
 * Created by kirif on 01.09.2017.
 */
public class FacebookPopup {

    private final WebDriver driver;

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "pass")
    private WebElement passwordField;

    @FindBy(id = "loginbutton")
    WebElement loginButton;

    @FindBy(xpath = ".//button[contains(@name,'__CONFIRM__')]")
    WebElement continueButton;

    public FacebookPopup(WebDriver driver){
        this.driver = driver;
    }

    public void typeEmail(String emailText){
        emailField.sendKeys(emailText);
    }

    public void typePassword(String passText){
        passwordField.sendKeys(passText);
    }

    public void loginFacebook(String emailText, String passText){
        WaiterService.waitElementClicable(loginButton);
        typeEmail(emailText);
        typePassword(passText);
        loginButton.click();
//        continueButton.click();
    }

    public void clickContinueButton(){
        continueButton.click();
    }
}
