package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utility.services.WaiterService;

import static utility.services.WebElementService.clickOnElement;
import static utility.services.WebElementService.sendKeys;


/**
 * Created by kirif on 27.08.2017.
 */
public class AuthPage {

    private WebDriver driver;

    @FindBy(xpath = "//input[@type='email']")
    public WebElement emailField;

    @FindBy(xpath = "//input[@type='password']")
    public WebElement password;

    @FindBy(xpath = "//*[contains(@id,'continue')]//button")
    public WebElement continueBtn;

    @FindBy(xpath = "//*[contains(@id,'login')]//button")
    public WebElement logInBtn;

    @FindBy(xpath = "//*[contains(@class,'button_bg_facebook')]")
    public WebElement facebookBtn;

    public AuthPage(WebDriver driver) {
        this.driver = driver;
    }

    public void typeEmail(String emailText){
        emailField.sendKeys(emailText);
    }

    public void typePassword(String passText) {
        password.sendKeys(passText);
    }

    public void login(String email, String pass){
        WaiterService.waitElementClicable(continueBtn);
        typeEmail(email);
        continueBtn.click();
        WaiterService.waitElementClicable(password);
        typePassword(pass);
        clickOnElement(logInBtn,"SignIn Button", driver);
    }

    public void waitFacebookPopup() {
        WaiterService.waitElementClicable(logInBtn);
    }

    public void clickOnFacebookBtn() {
        clickOnElement(facebookBtn, "Facebook Button", driver);
    }

    public boolean isAuthFieldVisible() {
        WaiterService.waitElementClicable(emailField);
        return emailField.isDisplayed();
    }
}
