package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utility.services.WaiterService;

import static utility.services.WaiterService.waitElementClicable;
import static utility.services.WebElementService.clickOnElement;


/**
 * Created by kirif on 01.09.2017.
 */
public class CabinetPage {

    private final WebDriver driver;

    public CabinetPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = "app-account-menu")
    private WebElement accountMenu;

    @FindBy(id = "header-signin-link")
    private WebElement signInLink;

    @FindBy(xpath = ".//button/span[@class='app-sign-out-btn__inner']")
    WebElement signOutButton;

    @FindBy(xpath = "//*[contains(@class,'tm-quark-popup__close-btn')]")
    public WebElement closePopupBtn;

    public boolean isAccountMenuVisible() {
        WaiterService.waitElementClicable(accountMenu);
        return accountMenu.isDisplayed();
    }

    public void clickOnAccountMenu() {
        WaiterService.waitElementClicable(accountMenu);
        accountMenu.click();
    }

    public void clickToSignOutButton() {
        WaiterService.waitElementClicable(signOutButton);
        signOutButton.click();
    }

    public void closeBetaPopup() {
        WaiterService.waitElementClicable(closePopupBtn);
        clickOnElement(closePopupBtn, "Close Beta Popup Button", driver);
    }
}

