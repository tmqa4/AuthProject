package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utility.services.WaiterService;

import static utility.services.WebElementService.clickOnElement;



/**
 * Created by kirif on 27.08.2017.
 */
public class IndexPage {

    public static WebDriver driver;

    @FindBy(id = "menu-favorites")
    public WebElement headHeart;

    @FindBy(id = "header-signin-link")
    public WebElement signInLink;

    @FindBy(xpath = "//*[contains(@id,'modalPop')]//div[contains(@class,'icon-close')]")
    public WebElement closeBannerBtn;

    @FindBy(xpath = "//span[contains(@class,'js-close-banner')]")
    public WebElement banner;


    public IndexPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnSignInBtn() {
        WaiterService.waitElementClicable(banner);
        clickOnElement(signInLink, "SignIn Button", driver);
    }

    public void closeBanner() {
        WaiterService.waitElementClicable(closeBannerBtn);
        clickOnElement(closeBannerBtn, "Close Banner", driver);
        WaiterService.waitElementClicable(signInLink);
    }

}