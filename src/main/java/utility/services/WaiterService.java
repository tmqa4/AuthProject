package utility.services;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Created by kirif on 27.08.2017.
 */
public class WaiterService {

    private static WebDriver driver;

    public WaiterService(WebDriver driver){
        this.driver=driver;
    }

    public static void waitElementClicable(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitElementPresented(String element){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(element)));
    }

    public void waitURL(String url){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.urlContains(url));
    }

    public void waitURL2(String url){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until((WebDriver webDriver) -> driver.getCurrentUrl().contains(url));
    }

}
