package utility.services;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.testng.Assert.assertTrue;

/**
 * Created by kirif on 27.08.2017.
 */
public class WebElementService {

    private WebDriver driver;

    public WebElementService(WebDriver driver){
        this.driver=driver;
    }

    public static void clickOnElement(WebElement element, String elementName, WebDriver driver) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (TimeoutException ex) {
            return;
        }
        try {
            element.click();
        } catch (NoSuchElementException e) {
            assertTrue(false, "\"" + elementName + "\" was not found on page after timeout.");
        } catch (StaleElementReferenceException e) {
            element.click();
        }
    }

    public static void sendKeys(WebElement element, String elementName, String inputText){
        try {
            element.sendKeys(inputText);
        }
        catch (NoSuchElementException e){
            assertTrue(false, "\"" + elementName + "\" was not found on page after timeout.");
        }
        catch (ElementNotVisibleException e){
            assertTrue(false, "\"" + elementName + "\" was not visible.");
        }
    }

    public void goTo(String url){
        driver.get(url);
    }

    public void switchToLastWindow(){
        driver.getWindowHandles().forEach(driver.switchTo()::window);
    }

}
